var mongoose = require('mongoose');

var BranchSchema = new mongoose.Schema({
  name: String,
  country: String,
  state: String,
  city: String,
  manager: {type: String, default: 'Josue Zatarain'},
  latitude: String,
  longitude: String
});

module.exports = mongoose.model('Branch', BranchSchema);
