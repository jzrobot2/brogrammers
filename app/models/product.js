var mongoose = require('mongoose');

var ProductSchema = new mongoose.Schema({
  name: String,
  price: Number,
  description: String,
  imgUrl: String,
  available: {type: Boolean, default: true}
});

module.exports = mongoose.model('Product', ProductSchema);
