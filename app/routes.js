var mailRouter = require('./routers/mail.router.js');
var usersRouter = require('./routers/user.router.js');
var resgistryRouter = require('./routers/registration.router.js');
var adminRouter = require('./routers/admin.router.js');
var branchRouter = require('./routers/branch.router.js');
var productRouter = require('./routers/product.router.js');
var facebookEventRouter = require('./routers/facebookEvents.router.js');

function router(app) {
    app.use('/contact', mailRouter);
    app.use('/users', usersRouter);
    app.use('/register', resgistryRouter);
    app.use('/admin', adminRouter);
    app.use('/branch', branchRouter);
    app.use('/product', productRouter);
    app.use('/facebookEvents', facebookEventRouter);
    // frontend routes =========================================================
    // route to handle all angular requests
}

module.exports = router;
