const router = require("express").Router();
const Branch = require('../models/branch.js');

router.get('/', getAllBranches);
router.get('/:id', getBranch);
router.post('/', addBranch);
router.put('/:id', updateBranch);
router.delete('/:id', deleteBranch);

function getAllBranches(req, res) {
    Branch.find(function(err, data) {
        if (err) {
            res.send(err);
        }
        res.json(data);
    });
}

function addBranch(req, res) {
    Branch.create(req.body, function(err, branch) {
        if (err) {
            res.send(err);
        }
        res.send(branch);
    });
}

function getBranch(req, res) {
    Branch.findById(req.params.id, function(err, branch) {
        if (err) {
            res.send(err);
        }
        res.json(branch);
    });
}

function updateBranch(req, res) {
    Branch.updateById({
        _id: req.params.id
    }, function(err, branch) {
        if (err) {
            res.send(err);
        }
        res.send(branch);
    });
}

function deleteBranch(req, res) {
    Branch.remove({
        _id: req.params.id
    }, function(err, branch) {
        if (err) {
            res.send(err);
        }
        res.send(branch);
    });
}

module.exports = router;
