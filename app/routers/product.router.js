const router = require("express").Router();
const Product = require('../models/product.js');

router.get('/', getAllProducts);
router.get('/:id', getProduct);
router.post('/', addProduct);
router.put('/:id', updateProduct);
router.delete('/:id', deleteProduct);

function getAllProducts(req, res) {
    Product.find(function(err, data) {
        if (err) {
            res.send(err);
        }
        res.json(data);
    });
}

function addProduct(req, res) {
    Product.create(req.body, function(err, product) {
        if (err) {
            res.send(err);
        }
        res.send(product);
    });
}

function getProduct(req, res) {
    Product.findById(req.params.id, function(err, product) {
        if (err) {
            res.send(err);
        }
        res.json(product);
    });
}

function updateProduct(req, res) {
    Product.updateById({
        _id: req.params.id
    }, function(err, product) {
        if (err) {
            res.send(err);
        }
        res.send(product);
    });
}

function deleteProduct(req, res) {
    Product.remove({
        _id: req.params.id
    }, function(err, product) {
        if (err) {
            res.send(err);
        }
        res.send(product);
    });
}

module.exports = router;
