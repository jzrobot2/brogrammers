const router = require("express").Router();
const User = require('../models/user');
var sess;

router.get('/', checkSession);
router.post('/login', login);
router.get('/logout', logout);

router.post('/register', register);

function register(req, res) {
    User.register(new User({
            username: req.body.username
        }),
        req.body.password,
        function(err, account) {
            if (err) {
                return res.status(500).json({
                    err: err
                });
            }
            passport.authenticate('local')(req, res, function() {
                return res.status(200).json({
                    status: 'Registration successful!'
                });
            });
        });
};

function checkSession(req, res, next) {
    sess = req.session;
    if (sess.username) {
        /*User.find({
            username: sess.username
        }, function(err, user) {
            if(err){
                res.send(err);
            }
            res.json(user);
        });*/
        res.redirect('/#home');
    }
    else {
        res.redirect('/#login');
    }
}

function login(req, res) {
    sess = req.session;
    sess.username = req.body.username;
    res.end('done');
}

function logout(req, res) {
    req.session.destroy(function(err) {
        if (err) {
            console.log(err);
        }
        else {
            res.redirect('/');
        }
    });
}

module.exports = router;
