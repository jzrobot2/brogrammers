const router = require("express").Router();
const User = require('../models/user');

router.get('/', getAllUsers);
router.get('/:id', getUser);
router.post('/', addUser);
router.put('/:id', updateUser);
router.delete('/:id', deleteUser);

function getAllUsers(req, res){
    User.find(function(err, data){
        if(err){
            res.send(err);
        }
        res.json(data);
    });
}
function addUser(req, res){
    User.create(req.body, function(err, user){
        if(err){
            res.send(err);
        }
        res.send(user);
    });
}
function getUser(req, res){
    User.findById(req.params.id, function(err, user){
        if(err){
            res.send(err);
        }
        res.json(user);
    });
}
function updateUser(req, res) {
    User.updateById({
        _id: req.params.id
    }, function(err, user){
        if(err){
            res.send(err);
        }
        res.json(user);
    });
}
function deleteUser(req, res) {
    User.Remove({
        _id: req.params.id
    }, function(err, user){
        if(err){
            res.send(err);
        }
        res.json(user);
    });
}
    
module.exports = router;