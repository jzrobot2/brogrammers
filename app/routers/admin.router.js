const router = require("express").Router();
const path = require('path');

router.get('/', showAdmin);

function showAdmin(req, res){
    res.sendFile('index.html', {root: './public/backend'});
}

module.exports = router;