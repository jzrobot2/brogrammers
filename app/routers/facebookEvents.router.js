const router = require("express").Router();
var EventSearch = require("/home/bitnami/apps/brogrammers/node_modules/facebook-events-by-location-core");

router.post('/', getEvents);
// router.set("x-powered-by", false);
// router.set("etag", false);

function getEvents(req, res){
    var options = {};
    options.accessToken = '1732994253693217|f75olQdGYem8c6LE9XKW8DoWyog';
    if(!req.query.lat || !req.query.lng) {
        res.status(500).json({message: "Please specify lat and lng parameters"});
    } else {
        options.lat = req.query.lat;
        options.lng = req.query.lng;
        options.distance = req.query.distance || 100;
        options.sort = req.query.sort;
        options.until = req.query.until;
    }
    var es = new EventSearch(options);
    
    es.search().then(function(response){
        res.json(response);
    }).catch(function (er){
        res.status(500).json(er);
    });
}

module.exports = router;