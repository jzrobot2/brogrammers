const router = require("express").Router();
const nodemailer = require('nodemailer');

router.post('/', contactForm);

function contactForm(req, res) {
    var transporter = nodemailer.createTransport({
        service: 'Gmail',
        auth: {
            user: '', // Your email id
            pass: '' // Your password
        }
    });
    var mailOptions = {
        from: '"Fred Foo ?" <foo@blurdybloop.com>', // sender address
        to: '', // list of receivers
        subject: 'Email Example', // Subject line
        text: 'text testing'
    };
    transporter.sendMail(mailOptions, function(error, info){
        if(error){
            res.json(error);
        }else{
            console.log('Message sent: ' + info.response);
            res.json({yo: info.response});
        };
    });
}

module.exports = router;

