const router = require("express").Router();
const Team = require('../models/team.js');

router.get('/', getAllTeams);
router.get('/:id', getTeam);
router.post('/', addTeam);
router.put('/:id', updateTeam);
router.delete('/:id', deleteTeam);

function getAllTeams(req, res){
    Team.find(function(err, data){
        if(err){
            res.send(err);
        }
        res.json(data);
    });
}
function addTeam(req, res){
    Team.create(req.body, function(err, team){
        if(err){
            res.send(err);
        }
        res.send(team);
    });
}
function getTeam(req, res){
    Team.findById(req.params.id, function(err, team){
        if(err){
            res.send(err);
        }
        res.send(Team);
    });
}
function updateTeam(req, res) {
    Team.updateById({
        _id: req.params.id
    }, function(err, team){
        if(err){
            res.send(err);
        }
        res.send(team);
    });
}
function deleteTeam(req, res) {
    Team.Remove({
        _id: req.params.id
    }, function(err, team){
        if(err){
            res.send(err);
        }
        res.send(team);
    });
}
    
module.exports = router;