// set up ======================================================================
var express = require('express');
var app = express(); // create our app w/ express
var path = require('path');
var favicon = require('serve-favicon');
var port = process.env.PORT || 8080; // set the port
var database = require('./config/database'); // load the database config
var morgan = require('morgan'); // log requests to the console (express4)
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser'); // pull information from HTML POST (express4)
var methodOverride = require('method-override'); // simulate DELETE and PUT (express4)
var mongoose = require('mongoose'); // mongoose for mongodb
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
mongoose.Promise = global.Promise;

// configuration ===============================================================
// passport config
var User = require('./app/models/user');
passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

mongoose.connect(database.remoteUrl) // Connect to local MongoDB instance
    .then(() => console.log('connection succesful'))
    .catch((err) => console.error(err));

app.use(morgan('dev')); // log every request to the console
app.use(bodyParser.urlencoded({
    'extended': 'true'
})); // parse application/x-www-form-urlencoded

app.use(require('express-session')({
    secret: '715823',
    resave: true,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());

app.use(bodyParser.json()); // parse application/json
app.use(bodyParser.json({
    type: 'application/vnd.api+json'
})); // parse application/vnd.api+json as json
app.use(methodOverride('X-HTTP-Method-Override')); // override with the X-HTTP-Method-Override header in the request
//app.use(favicon(path.join(__dirname, 'public', 'favicons/favicon.ico')));
app.use(cookieParser());
app.use(express.static('./public')); // set the static files location /public/img will be /img for users

// routes ======================================================================
require('./app/routes.js')(app);

// listen (start app with node server.js) ======================================
app.listen(port);
console.log("App listening on port " + port);
