/*global angular*/
angular
    .module('red-ribbon')
    .factory('Branch', Branch);

Branch.inject = ['$http'];

function Branch($http) {
    var service = {
        getBranches: getBranches,
        createBranch: createBranch,
        deleteBranch: deleteBranch
    };

    return service;

    function getBranches() {
        return $http.get('/branch');
    }

    function createBranch(data) {
            return $http.post('/branch', data);
    }

    function deleteBranch(id){
        return $http.delete('/branch/' + id);
    }
}
