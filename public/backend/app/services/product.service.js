/*global angular*/
angular
    .module('red-ribbon')
    .factory('Product', Product);

Product.inject = ['$http'];

function Product($http) {
    var service = {
        getProducts: getProducts,
        createProduct: createProduct,
        deleteProduct: deleteProduct
    };

    return service;

    function getProducts() {
        return $http.get('/product');
    }

    function createProduct(data) {
        return $http.post('/product', data);
    }

    function deleteProduct(id) {
        return $http.delete('/product/' + id);
    }
}
