/*global angular, $, Materialize*/
var dependencies = [
    'ngAria',
    'ngRoute',
    'ngMessages',
    'ngAnimate',
    'angular-loading-bar'
];
angular
    .module("red-ribbon", dependencies)
    .config(router)
    .run(run);

function router($routeProvider) {
    $routeProvider
    // route for the home page
        .when('/', {
            templateUrl: 'backend/app/templates/home.template.html',
            controller: 'homeController',
            controllerAs: 'vm'
        })
        .when('/branches', {
            templateUrl: 'backend/app/templates/branches/branches.template.html',
            controller: 'branchController',
            controllerAs: 'vm'
        })
        .when('/branches/create', {
            templateUrl: 'backend/app/templates/branches/create.template.html',
            controller: 'branchController',
            controllerAs: 'vm'
        })
        .when('/products', {
            templateUrl: 'backend/app/templates/products/products.template.html',
            controller: 'productController',
            controllerAs: 'vm'
        })
        .when('/products/create', {
            templateUrl: 'backend/app/templates/products/create.template.html',
            controller: 'productController',
            controllerAs: 'vm'
        })
        .otherwise({
            templateUrl: 'backend/app/views/404.html'
        });
}

function run($rootScope){
    $rootScope.$on('toast', function(event, args){
        Materialize.toast(args, 5000);
    });
}