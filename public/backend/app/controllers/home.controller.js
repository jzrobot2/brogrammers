/*global angular*/
angular
    .module("red-ribbon")
    .controller('homeController', HomeController);

HomeController.$inject = ['$http', '$scope'];

function HomeController($http, $scope){
    var vm = this;

    initializeComponents();
    function initializeComponents(){
        getDailyBingImage();
    }
    
    function getDailyBingImage(){
        $http.get('https://api.nasa.gov/planetary/apod?api_key=59ic3XmFOKWV8EbBOLHLOclSDYmCDgaO4uRawAbh')
        .then(function(response){
            vm.dailyImage = {
                url: response.data.url,
                explanation: response.data.explanation,
                title: response.data.title
            };
        }, function(){
            $scope.$emit('toast', 'Unable to contact the NASA');
        });
    }
}
