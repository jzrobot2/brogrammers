/*global angular*/
angular
    .module("red-ribbon")
    .controller('productController', ProductController);

ProductController.$inject = ['Product', '$scope'];

function ProductController(Product, $scope) {
    var vm = this;
    vm.save = save;
    vm.loadProducts = loadProducts;
    vm.deleteProduct = deleteProduct;

    initializeComponents();

    function initializeComponents() {

    }
    
    function save(data){
        Product.createProduct(data).then(function(){
            $scope.$emit('toast', 'New product was Created');
        }, function(){
            $scope.$emit('toast', 'Unable to create product');
        });
    }
    
    function deleteProduct(id){
        Product.deleteProduct(id).then(function(){
            $scope.$emit('toast', 'Product deleted');
            vm.products = [];
            loadProducts();
        }, function(){
            $scope.$emit('toast', 'Unable to delete product');
        });
    }
    
    function loadProducts(){
        Product.getProducts().then(function(response){
            vm.products = response.data;
        }, function(){
            $scope.$emit('toast', 'Unable to load products');
        });
    }

}
