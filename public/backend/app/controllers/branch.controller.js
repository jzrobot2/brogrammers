/*global angular*/
angular
    .module("red-ribbon")
    .controller('branchController', BranchController);

BranchController.$inject = ['Branch', '$scope', '$location'];

function BranchController(Branch, $scope, $location) {
    var vm = this;
    vm.save = save;
    vm.loadBranches = loadBranches;
    vm.deleteBranch = deleteBranch;

    initializeComponents();

    function initializeComponents() {

    }
    
    function save(data){
        Branch.createBranch(data).then(function(){
            $scope.$emit('toast', 'New branch was Created');
        }, function(){
            $scope.$emit('toast', 'Unable to create branch');
        });
    }
    
    function deleteBranch(id){
        Branch.deleteBranch(id).then(function(){
            $scope.$emit('toast', 'Branch deleted');
            vm.branches = [];
            loadBranches();
        }, function(){
            $scope.$emit('toast', 'Unable to delete branch');
        });
    }
    
    function loadBranches(){
        Branch.getBranches().then(function(response){
            vm.branches = response.data;
        }, function(){
            $scope.$emit('toast', 'Unable to load braches');
        });
    }

}
