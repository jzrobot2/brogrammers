/*global angular, $*/
angular
    .module("red-ribbon")
    .directive('myHeader', myHeader);

myHeader.$inject = [];

function myHeader(){
    var directive = {
        restrict: 'EA',
        templateUrl: 'app/templates/frontend-header.template.html',
        scope: true,
        controller: MyHeaderController,
        controllerAs: 'vm',
        bindToController: true,
        link: linkFunc
    };

    return directive;

    function linkFunc(scope, el, attr, vm){
    }
}


function MyHeaderController() {
    var vm = this;
}
