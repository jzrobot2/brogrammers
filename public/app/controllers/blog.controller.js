angular
  .module("red-ribbon")
  .controller('blogController', BlogController);

BlogController.$inject = ['$http', 'Branch', 'facebookEvents'];

function BlogController($http, Branch, facebookEvents) {
  var vm = this;
  vm.getBranchEvents = getBranchEvents;
  vm.selectedBranch;
  vm.branchName = '';
  vm.branches = {};
  vm.events = [];
  Branch.getBranches().then(function(response) {
    vm.branches = response.data;
    // vm.events = Events.getEvents(vm.branches)
    // console.log(vm.branches);
  }, function(er) {
  });

  function getBranchEvents() {
    if (vm.selectedBranch || vm.selectedBranch !== '') {
      var branchObj = JSON.parse(vm.selectedBranch);
      facebookEvents.getEvents(branchObj.latitude, branchObj.longitude, 1000).then(function(response) {
        vm.branchName = branchObj.name;
        vm.events = response.data.events;
      });
    }

  }
}
