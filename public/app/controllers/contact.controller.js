angular
    .module('red-ribbon')
    .controller('contactController', ContactController);

ContactController.$inject = ['Branch'];

function ContactController(Branch) {
    var vm = this;
    vm.branches;
    Branch.getBranches().then(function(response) {
        vm.branches = response.data;
    });
    initializeComponents();

    function initializeComponents() {}

}
