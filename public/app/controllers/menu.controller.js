/*global angular*/
angular
    .module("red-ribbon")
    .controller('menuController', MenuController);

MenuController.$inject = ['Product'];

function MenuController(Product) {
    var vm = this;
    vm.productsBulks = [];
    initializeComponents();

    function initializeComponents() {
        loadProducts();
    }

    function loadProducts() {
        Product.getProducts().then(function(response) {
            var bulk = [];
            for(var i=1; i<response.data.length; i++){
                bulk.push(response.data[i]);
                if(i % 3 == 0){
                    vm.productsBulks.push(bulk);
                    bulk = [];
                }else if ((i+1) === response.data.length){
                    vm.productsBulks.push(bulk);
                }
            }
        });
    }
}
