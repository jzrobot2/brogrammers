/*global angular*/
var dependencies = [
    'ngAria',
    'ngRoute',
    'ngMessages',
    'ngAnimate',
    'angular-loading-bar'
];
angular
    .module("red-ribbon", dependencies)
    .config(router);

function router($routeProvider) {
    $routeProvider
    // route for the home page
        .when('/', {
            templateUrl: 'app/templates/home.template.html',
            controller: 'homeController',
            controllerAs: 'vm'
        })
        .when('/home', {
            templateUrl: 'app/templates/home.template.html',
            controller: 'homeController',
            controllerAs: 'vm'
        })
        .when('/blog', {
            templateUrl: 'app/templates/blog.template.html',
            controller: 'blogController',
            controllerAs: 'vm'
        })
        .when('/menu', {
            templateUrl: 'app/templates/menu.template.html',
            controller: 'menuController',
            controllerAs: 'vm'
        })
        .when('/reservation', {
            templateUrl: 'app/templates/reservation.template.html',
            controller: 'reservationController',
            controllerAs: 'vm'
        })
        .when('/contact', {
            templateUrl: 'app/templates/contact.template.html',
            controller: 'contactController',
            controllerAs: 'vm'
        })
        .otherwise({
            templateUrl: 'app/views/404.html'
        });
}
