angular
    .module('red-ribbon')
    .factory('facebookEvents', facebookEvents);
    
    facebookEvents.$inject = ['$http'];
    
    function facebookEvents($http){
        var service = {
            getEvents: getEvents
        };
        
         return service;
        
        // Query reference: events/?lat=25.6766&lng=-100.3133,14&distance=1000
        function getEvents(lat, lng, distance){
            return $http.post('/facebookEvents/?lat=' + lat + '&lng='+ lng + '&distance=' + distance);
        }
    }